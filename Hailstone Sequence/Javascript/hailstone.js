#!/usr/bin/env node

/* The Collatz Conjecture states that any number sequence
 * will eventually terminate in 1 if sequence with the operations
 * such that if even, next term is n/2 and that if odd, next term
 * is 3n + 1.
 * These sequences are called hailstone sequences.
 *
 * This program will find the hailstone sequence of any number
 * passed as the first commandline argument, as well as the overall
 * length of the sequence.
*/


var hailstone = function(num) {
	var result = num;
	var sequence = [num];

	if (num <= 0) {
		console.log("Please enter a positive integer > 0.");
	}

	while (result != 1) {
		if (result % 2 === 0) {
			result = result / 2;
		} else {
			result = (3 * result) + 1;
		}

		sequence.push(result);
	}

	return sequence;
}

if (process.argv[1] == __filename) {
	if (process.argv[2] === undefined) {
		console.log("ERROR: No Number provided. Exiting.");
		process.exit(1);
	}
	console.log(hailstone(process.argv[2]));
	console.log("Overall length: " + hailstone(process.argv[2]).length);
} else {
	module.exports = hailstone;
}