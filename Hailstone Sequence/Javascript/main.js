#!/usr/bin/env node

var hailstone = require('./hailstone');
var maxNum = 0;
var maxLength = 0;

for (var i = 1; i < 100000; i++) {
	var results = hailstone(i).length;

	if (results > maxLength) {
		maxNum = i;
		maxLength = results;
	}
}

console.log("Largest hailstone chain is " + maxLength + " units long, from number " + maxNum);