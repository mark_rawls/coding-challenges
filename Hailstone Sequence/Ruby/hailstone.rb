#!/usr/bin/env ruby

def hailstone(n)
	sequence = [n]
	result = n

	until result == 1
		if result % 2 == 0
			result = result / 2
		else
			result = result * 3 + 1
		end
		sequence << result
	end

	return sequence
end

if __FILE__ == $PROGRAM_NAME
	if ARGV[0].nil?
		puts "ERROR: No Number provided. Exiting."
		exit 1
	end

	hailstone(ARGV[0])
end