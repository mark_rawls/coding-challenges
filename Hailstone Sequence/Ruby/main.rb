#!/usr/bin/env ruby

require './hailstone'
maxNum = 0
maxLength = 0


1.upto(100000) do |i|
	result = hailstone(i)
	if result.length > maxLength
		maxLength = result.length
		maxNum = i
	end
end

puts "Longest hailstone chain was for #{maxNum}, with"
puts "a length of #{maxLength} units."