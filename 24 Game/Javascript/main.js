#!/usr/bin/env node

/* 24 Game:
 * Write a program that randomly chooses and displays four digits,
 * each from one to nine, with repetitions allowed. The program should
 * prompt for the player to enter an arithmetic expression using just those,
 * and all of those four digits, used exactly once each. The program should
 * check then evaluate the expression. The goal is for the player to enter an
 * expression that evaluates to 24.
*/

