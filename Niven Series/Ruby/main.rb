#!/usr/bin/env ruby

require './niven'
include Niven

series = []
i = 1

while series.length < 20
	if Niven::sequences?(i)
		series << i
	end
	i += 1
end

puts series

i = 1001

loop do
	if Niven::sequences?(i)
		puts i
		break
	end
	i += 1
end