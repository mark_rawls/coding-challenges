#!/usr/bin/env ruby

module Niven
	def sequences?(num)
		if num % split(num) == 0
			true
		else
			false
		end
	end

	def split(num)
		num.to_s.split('').map { |e| e.to_i }.inject { |sum,x| sum + x }
	end
end

if __FILE__ == $PROGRAM_NAME
	if ARGV[0].nil?
		puts "ERROR: No Number provided. Exiting."
		exit 1
	end

	puts Niven::sequences?(ARGV[0])
end