#!/usr/bin/env node

var niven = {};

niven.split = function(num) {
	// Ensure that argument is a string
	return num.toString().split('').map(function(val) {
		return parseInt(val, 10); // Turn all arguments into ints for safety
	}).reduce(function(a, b) {
		return a + b; // Sum them and return
	});
};

niven.sequences = function(num) {
	// Because Javascript is less clean than ruby, this is necessary
	if (num % niven.split(num) != 0) {
		return false; // Does it split evenly?
	} else {
		return true;
	}
};

if (__filename == process.argv[1]) { // Allow running this as main file with arguments
	if (process.argv[2] != undefined) {
		console.log(niven.sequences(process.argv[2]));
	} else {
		console.log("ERROR: Please specify a number in your argument");
	}
} else { // Otherwise just export the module to whoever is calling it
	module.exports = niven;
}