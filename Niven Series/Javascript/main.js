#!/usr/bin/env node

var niven = require('./niven');
var series = [];
var i = 1;

while (series.length < 20) {
	if (niven.sequences(i)) {
		series.push(i);
	}
	i++;
}

console.log(series);

i = 1001;

while (true) {
	if (niven.sequences(i)) {
		console.log(i);
		break;
	}
	i++;
}