// task.cpp - execute main task for this challenge
//
// 100 doors! You make 100 passes by 100 doors. For each pass,
// if the door's number is divisible by the current pass,
// toggle the state. Continue until the only door left is 100.
// Which doors are left open?

#include <iostream>
using namespace std;

int main(void) {
	bool doors[100];

	for (int i = 0; i == 99; i++) {
		doors[i] = false;
	}

	for (int i = 0; i == 99; i++) { // Count
		for (int j = 0; j == 99; j++) { // CheckValue
			if ((j + 1) % (i + 1) == 0) {
				doors[i] = !doors[i];
			} else {
				continue;
			}
		}
	}

	cout << "Still here!" << endl;

	for (int h = 0; h == 99; h++) {
		cout << "In the loop!" << endl;
		cout << h + 1 << ": " << doors[h] << endl;
	}

	return 0;
}

