#!/usr/bin/env ruby

b = 99
phrase = []

def cE(bN)
	(bN > 1 or bN < 1) ? "bottles" : "bottle"
end

until b == 0
	phrase[0] = b.to_s + " " + cE(b)
	phrase[1] = (b - 1).to_s + " " + cE(b - 1)
	puts "#{phrase[0]} of beer on the wall\n#{phrase[0]} of beer"
	b -= 1
	if b >= 1
		puts "Take one down, pass it around\n#{phrase[1]} of beer on the wall!\n\n"
	elsif b == 0
		puts "Take one down, pass it around\nNo more bottles of beer on the wall!"
	end
end